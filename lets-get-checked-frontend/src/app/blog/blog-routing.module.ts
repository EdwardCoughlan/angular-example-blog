import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostListComponent } from './container/post-list/post-list.component';
import { PostComponent } from './container/post/post.component';
import { PostsResolver } from './resolvers/posts.resolver';
import { PostResolver } from './resolvers/post.resolver';


const routes: Routes = [{
  path: '',
  component: PostListComponent,
  resolve: { posts: PostsResolver }
}, {
  path: ':id',
  component: PostComponent,
  resolve: { post: PostResolver }
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
