import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { PostListComponent } from './container/post-list/post-list.component';
import { PostComponent } from './container/post/post.component';
import { BlogpostComponent } from './components/blogpost/blogpost.component';
import { BlogpostsComponent } from './components/blogposts/blogposts.component';
import { CommentListComponent } from './container/comment-list/comment-list.component';
import { CommentDetailComponent } from './container/comment-detail/comment-detail.component';
import { CommentsSectionComponent } from './components/comments-section/comments-section.component';
import { CommentSectionComponent } from './components/comment-section/comment-section.component';
import { CommentFormComponent } from './components/comment-form/comment-form.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [PostListComponent, PostComponent, BlogpostComponent, BlogpostsComponent, CommentListComponent, CommentDetailComponent, CommentsSectionComponent, CommentSectionComponent, CommentFormComponent],
  imports: [
    CommonModule,
    BlogRoutingModule,
    ReactiveFormsModule
  ]
})
export class BlogModule { }
