import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogpostComponent } from './blogpost.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('BlogpostComponent', () => {
  let component: BlogpostComponent;
  let fixture: ComponentFixture<BlogpostComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlogpostComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogpostComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement
    component.post = {
      id: 1,
      author: 'author',
      content: 'content',
      description: 'description',
      publish_date: 'publish_date',
      slug: 'slug',
      title: 'title'
    }
    fixture.detectChanges();
  });

  it('should create contentView', () => {
    component.contentView = true;
    fixture.detectChanges();
    expect(component).toBeTruthy();
    const contentText = debugElement.query(By.css('.card-main')).nativeElement.innerText;
    expect(contentText).toBe('content');
  });

  it('should create multiView', () => {
    component.contentView = false;
    expect(component).toBeTruthy();
    const buttonText = debugElement.query(By.css('button')).nativeElement.innerText;
    expect(buttonText).toBe('Read');
    const descriptionText = debugElement.query(By.css('.card-main')).nativeElement.innerText;
    expect(descriptionText).toBe('description');
  });
});
