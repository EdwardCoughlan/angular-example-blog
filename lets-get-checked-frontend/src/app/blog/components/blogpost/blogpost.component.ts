import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../../models/post';

@Component({
  selector: 'app-blogpost',
  template: `
    <article class="card">
      <div class="card-header">
        <ng-container *ngIf="contentView; else link">
          <div class="title">
            {{post.title}}
          </div>
        </ng-container>
        <ng-template #link>
          <a [routerLink]="post.slug" class="title">
            {{post.title}}
          </a>
        </ng-template>
        <div class="title sub-title">{{post.author}}</div>
        <div> {{post.publish_date}}</div>
      </div>
      <ng-container *ngIf="contentView; else description">
        <div class="card-main" [innerHTML]="post.content"></div> 
        <app-comment-list [postId]="post.id"></app-comment-list>
        <app-comment-form [postId]="post.id"></app-comment-form>
      </ng-container>
      <ng-template #description>
        <div class="card-main"><p>{{post.description}}</p></div> 
        <div class="card-footer">
          <button class="title" [routerLink]="post.slug">
              Read
          </button>
        </div>
      </ng-template>
    </article>
  `,
  styleUrls: ['./blogpost.component.scss']
})
export class BlogpostComponent implements OnInit {
  @Input() post: Post;
  @Input() contentView: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

}
