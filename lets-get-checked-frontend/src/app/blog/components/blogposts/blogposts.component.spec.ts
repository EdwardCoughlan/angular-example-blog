import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogpostsComponent } from './blogposts.component';
import { Post } from '../../models/post';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('BlogpostsComponent', () => {
  let component: BlogpostsComponent;
  let fixture: ComponentFixture<BlogpostsComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BlogpostsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogpostsComponent);
    component = fixture.componentInstance;
    component.posts = [
      {
        id: 1,
        author: 'Edward',
        title: 'Post 1',
        slug: 'Post-1',
        content: 'Text',
        description: 'Text',
        publish_date: "2020-11-11",
      }, {
        id: 2,
        author: 'Edward',
        title: 'Post 2',
        slug: 'Post-2',
        content: 'Text',
        description: 'Text',
        publish_date: "2020-11-11",
      }
    ]
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    const post1 = debugElement.query(By.css('[data-testid="Post-1"]')).nativeElement;
    const post2 = debugElement.query(By.css('[data-testid="Post-2"]')).nativeElement;
    expect(post1).toBeTruthy();
    expect(post2).toBeTruthy();
  });
});
