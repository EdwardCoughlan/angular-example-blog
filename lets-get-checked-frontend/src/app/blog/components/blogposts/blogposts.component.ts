import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../../models/post';

@Component({
  selector: 'app-blogposts',
  template: `
    <div class="blogpost-grid">
      <section *ngFor="let post of posts">
        <app-blogpost [post]="post" [attr.data-testid]="post.slug"></app-blogpost>
      </section>
    </div>
  `,
  styleUrls: ['./blogposts.component.scss']
})
export class BlogpostsComponent implements OnInit {
  @Input() posts: Post[];
  constructor() { }

  ngOnInit(): void {
  }

}
