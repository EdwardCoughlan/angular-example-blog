import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentFormComponent } from './comment-form.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AppModule } from 'src/app/app.module';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('CommentFormComponent', () => {
  let component: CommentFormComponent;
  let fixture: ComponentFixture<CommentFormComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommentFormComponent],
      imports: [CommonModule, ReactiveFormsModule, AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentFormComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    component.postId = 1;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('I can enter form value and I will see the button', () => {
    const commentInputElement = debugElement.query(By.css('[data-testid="comment-input"]')).nativeElement;
    const commentUsernameElement = debugElement.query(By.css('[data-testid="username-input"]')).nativeElement;
    commentInputElement.value = "comment";
    commentUsernameElement.value = "username";
    commentInputElement.dispatchEvent(new Event('input'));
    commentUsernameElement.dispatchEvent(new Event('input'));

    expect(component.comment.value).toBe('comment');
    expect(component.userName.value).toBe('username');


    fixture.detectChanges();
    const commentButtonElement = debugElement.query(By.css('[data-testid="comment-button"]')).nativeElement;
    expect(commentButtonElement).toBeTruthy();
  })
});
