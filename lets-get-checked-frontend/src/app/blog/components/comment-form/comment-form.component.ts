import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Comment } from '../../models/comment';
import { CommentService } from '../../services/comment.service';
import { SharedCommentsService } from '../../services/shared-comments.service';

@Component({
  selector: 'app-comment-form',
  template: `
      <form [formGroup]="commentForm" (ngSubmit)="submitHandler()">
        <div class="input-group">
          <input formControlName="comment" placeholder="Say something..." [attr.data-testid]="'comment-input'">
          <span *ngIf="comment.touched && comment.invalid">
            <ng-container *ngIf="comment.errors?.maxLength; else defaultError">
              Seems like you have a lot to say.
            </ng-container >
            <ng-template #defaultError>
              Gonna need a comment here.
            </ng-template>
          </span> 
        </div>
        <div class="input-group">
          <input formControlName="userName" placeholder="username" [attr.data-testid]="'username-input'">
          <span *ngIf="userName.touched && userName.invalid">
            <ng-container *ngIf="userName.errors?.maxLength; else defaultError">
              Sorry your name is too long.
            </ng-container >
            <ng-template #defaultError>
              Gonna need a name here.
            </ng-template>
          </span> 
        </div>
        <span *ngIf="apiError">
            <ng-container *ngIf="userName.errors?.maxLength; else defaultError">
              Sorry your name is too long.
            </ng-container >
            <ng-template #defaultError>
              Gonna need a name here.
            </ng-template>
          </span> 
        <button *ngIf="comment.valid && userName.valid" [disabled]="loading"  [attr.data-testid]="'comment-button'">
          Submit
        </button>
      </form>
  `,
  styleUrls: ['./comment-form.component.scss']
})
export class CommentFormComponent implements OnInit {
  @Input() public postId: number;
  public loading: boolean = false;
  public commentForm: FormGroup;
  public apiError: boolean = false;
  constructor(private fb: FormBuilder, private service: SharedCommentsService) { }

  ngOnInit(): void {

    this.commentForm = this.fb.group({
      comment: ['', [Validators.required, Validators.minLength(1), Validators.max(250)]],
      userName: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]]
    })
  }

  get comment() {
    return this.commentForm.get('comment');
  }

  get userName() {
    return this.commentForm.get('userName');
  }

  async submitHandler() {
    if (!this.loading) {
      this.loading = true;
      const commentFormValue = this.commentForm.value;
      this.service.addComment(this.postId, commentFormValue.userName, commentFormValue.comment).pipe().subscribe(res => {
        this.commentForm.reset();
        this.service.loadComments(this.postId);
        this.loading = false;
      }, error => {
        console.log(error);
        this.apiError = true;
        this.loading = false;
      });
    }
  }
}
