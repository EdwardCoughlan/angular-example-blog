import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentSectionComponent } from './comment-section.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('CommentSectionComponent', () => {
  let component: CommentSectionComponent;
  let fixture: ComponentFixture<CommentSectionComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommentSectionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentSectionComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    component.comment = {
      content: 'content',
      date: 'date',
      id: 1,
      parent_id: 1,
      postId: 1,
      user: 'user'
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    const commentUsernameElement = debugElement.query(By.css('[data-testid="comment-1"]')).nativeElement;
    expect(commentUsernameElement).toBeTruthy();
    const content = debugElement.query(By.css('[data-testid="comment-1-content"]')).nativeElement;
    console.log(content, 'Content');
    expect(content.innerText).toBe('content')
    const date = debugElement.query(By.css('[data-testid="comment-1-date"]')).nativeElement;
    expect(date.innerText).toBe('date')
    const user = debugElement.query(By.css('[data-testid="comment-1-user"]')).nativeElement;
    expect(user.innerText).toBe('user')
  });

});
