import { Component, OnInit, Input } from '@angular/core';
import { Comment } from '../../models/comment';

@Component({
  selector: 'app-comment-section',
  template: `
    <div class="comment" [attr.data-testid]="'comment-'+comment.id">
      <div class="comment-main" [attr.data-testid]="'comment-'+comment.id+'-content'">
          {{comment.content}}
      </div>
      <div class="user" [attr.data-testid]="'comment-'+comment.id+'-user'">
        {{comment.user}}
      </div>
      <div class="date" [attr.data-testid]="'comment-'+comment.id+'-date'">
        {{comment.date}}
      </div>
    </div>

  `,
  styleUrls: ['./comment-section.component.scss']
})
export class CommentSectionComponent implements OnInit {
  @Input() public comment: Comment;
  public image = 'assets/cookies.svg';
  public imageAlt = 'User image';
  constructor() { }

  ngOnInit(): void {
  }

}
