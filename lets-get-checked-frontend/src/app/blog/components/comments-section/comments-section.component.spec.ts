import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentsSectionComponent } from './comments-section.component';

describe('CommentsSectionComponent', () => {
  let component: CommentsSectionComponent;
  let fixture: ComponentFixture<CommentsSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommentsSectionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsSectionComponent);
    component = fixture.componentInstance;
    component.comments = [
      { id: 1, parent_id: 1, postId: 1, content: 'content1', date: 'date', user: 'user' },
      { id: 2, parent_id: 1, postId: 1, content: 'content2', date: 'date', user: 'user' },
      { id: 3, parent_id: 1, postId: 1, content: 'content3', date: 'date', user: 'user' },
      { id: 4, parent_id: 1, postId: 1, content: 'content4', date: 'date', user: 'user' },
    ]
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
