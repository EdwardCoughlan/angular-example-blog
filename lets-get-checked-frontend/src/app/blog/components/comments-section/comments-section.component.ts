import { Component, OnInit, Input } from '@angular/core';
import { Comment } from '../../models/comment';

@Component({
  selector: 'app-comments-section',
  template: `
    <div class="comments">
      <div class="title sub-title">
        Comments
      </div>
      <section *ngFor="let comment of comments">
        <app-comment-detail [comment]="comment"></app-comment-detail>
      </section>
    </div>
  `,
  styleUrls: ['./comments-section.component.scss']
})
export class CommentsSectionComponent implements OnInit {
  @Input() public comments: Comment[];
  constructor() { }

  ngOnInit(): void {
  }

}
