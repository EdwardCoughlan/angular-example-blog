import { Component, OnInit, Input } from '@angular/core';
import { Comment } from '../../models/comment';
@Component({
  selector: 'app-comment-detail',
  template: `
    <app-comment-section [comment]="comment"></app-comment-section>
  `,
  styleUrls: []
})
export class CommentDetailComponent implements OnInit {
  @Input() public comment: Comment;
  constructor() { }

  ngOnInit(): void {
  }

}
