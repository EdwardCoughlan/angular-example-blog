import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentListComponent } from './comment-list.component';
import { SharedCommentsService } from '../../services/shared-comments.service';
import { DebugElement } from '@angular/core';
import { Comment } from '../../models/comment';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpClientModule } from '@angular/common/http';
'rxjs/internal/Observable';

describe('CommentListComponent', () => {
  let component: CommentListComponent;
  let fixture: ComponentFixture<CommentListComponent>;
  let debugElement: DebugElement;

  let service: SharedCommentsService;
  let loadCommentsSpy: jasmine.Spy;
  let lastestCommentsSpy: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommentListComponent],
      imports: [HttpClientModule],
      providers: [SharedCommentsService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentListComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    service = debugElement.injector.get(SharedCommentsService);
    loadCommentsSpy = spyOn(service, 'loadComments').and.returnValue();
    service.lastestComments = of([
      { id: 1, postId: 1, parent_id: 1, content: 'content', date: 'date', user: 'user' }
    ] as Comment[])

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.comments[0]).toBeTruthy();
    expect(loadCommentsSpy).toHaveBeenCalled();
    expect(component.comments[0].content).toBe('content');
  });
});
