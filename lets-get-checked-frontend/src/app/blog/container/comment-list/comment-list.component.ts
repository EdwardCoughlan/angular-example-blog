import { Component, OnInit, Input } from '@angular/core';
import { Comment } from '../../models/comment';
import { CommentService } from '../../services/comment.service';
import { SharedCommentsService } from '../../services/shared-comments.service';

@Component({
  selector: 'app-comment-list',
  template: `
    <app-comments-section [comments]="comments"></app-comments-section>
  `,
  styleUrls: []
})
export class CommentListComponent implements OnInit {
  public comments: Comment[] = []
  @Input() postId: number;
  constructor(private service: SharedCommentsService) { }

  ngOnInit(): void {
    this.service.loadComments(this.postId);
    this.service.lastestComments.subscribe(res => this.comments = res);
  }

}
