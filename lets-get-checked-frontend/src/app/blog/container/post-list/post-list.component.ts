import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'app-post-list',
  template: `
   <app-blogposts [posts]="posts"></app-blogposts>
  `,
  styleUrls: []
})
export class PostListComponent implements OnInit {
  public posts: Post[] = [];
  constructor(private route: ActivatedRoute, private titleService: Title) { }

  ngOnInit(): void {
    this.route.data.pipe(map(data => data['posts'])).subscribe(res => this.posts = res)
    this.titleService.setTitle('Blogs');
  }

}
