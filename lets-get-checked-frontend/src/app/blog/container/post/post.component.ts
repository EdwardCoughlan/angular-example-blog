import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-post',
  template: `
    <app-blogpost [post]="post" [contentView]="true"></app-blogpost>
  `,
  styleUrls: []
})
export class PostComponent implements OnInit {
  public post: Post = new Post();
  constructor(private route: ActivatedRoute, private titleService: Title) {
  }

  ngOnInit(): void {
    this.route.data.pipe(map(data => data['post'])).subscribe(res => this.post = res);
    this.titleService.setTitle(this.post.title);
  }
}
