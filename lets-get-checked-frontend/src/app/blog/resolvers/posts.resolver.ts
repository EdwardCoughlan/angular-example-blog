import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Post } from '../models/post';
import { PostService } from '../services/post.service';


@Injectable({
    providedIn: 'root'
})
export class PostsResolver implements Resolve<Post[]> {
    constructor(private service: PostService) {
    }

    resolve() {
        return this.service.getPosts();
    }
}