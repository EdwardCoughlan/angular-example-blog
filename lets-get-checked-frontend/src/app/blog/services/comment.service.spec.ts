import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController }
  from '@angular/common/http/testing';

import { CommentService } from './comment.service';
import { Comment } from '../models/comment';
import { of } from 'rxjs';

const mockComments: Comment[] = [
  { id: 1, postId: 1, parent_id: 1, content: 'content', date: 'date', user: 'user' }
]

describe('Comment service', () => {
  let service: CommentService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(CommentService);
    httpTestingController = TestBed.get(HttpTestingController);
  });
  afterEach(() => {
    // httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
