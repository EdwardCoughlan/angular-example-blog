import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Comment } from '../models/comment';
import { map } from 'rxjs/operators';
const baseUrl = (postId: number) => `${environment.apiBaseUrl}/posts/${postId}/comments`;
@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  public getComments(postId: number): Observable<Comment[]> {
    return this.http.get<Comment[]>(baseUrl(postId)).pipe(map((comments: Comment[]) => comments.sort((commentA, commentB) => {
      const commentADate = new Date(commentA.date).getTime();
      const commmentBDate = new Date(commentB.date).getTime();
      return commentADate - commmentBDate;
    }).reverse()
    ));;
  }

  public postComment(postId: number, comment: Comment): Observable<Comment> {
    return this.http.post<Comment>(baseUrl(postId), comment);
  }
}
