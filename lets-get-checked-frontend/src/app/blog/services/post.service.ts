import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../models/post';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

const baseUrl = `${environment.apiBaseUrl}/posts`
@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  public getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(baseUrl).pipe(map((posts: Post[]) => posts.sort((postA, postB) => {
      const postADate = new Date(postA.publish_date).getTime();
      const postBDate = new Date(postB.publish_date).getTime();
      return postADate - postBDate;
    }).reverse()));
  }

  public getPost(slug: string): Observable<Post> {
    const sluggedPosts = this.http.get<Post[]>(`${baseUrl}?slug=${slug}`);
    return sluggedPosts.pipe(map((posts: Post[]) => posts.find(post => post.slug === slug)));
  }

  public postPost(post: Post): Observable<Post> {
    return this.http.post<Post>(`${baseUrl}`, post);
  }
}
