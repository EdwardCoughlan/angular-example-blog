import { TestBed } from '@angular/core/testing';

import { SharedCommentsService } from './shared-comments.service';
import { CommentService } from './comment.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of, Observable } from 'rxjs';
import { Comment } from '../models/comment';

const mockComments: Comment[] = [{ id: 1, postId: 1, parent_id: 1, content: 'content', user: 'user', date: 'date' }]

describe('SharedCommentsService', () => {
  let service: SharedCommentsService;
  let commentService: CommentService;
  let getCommentServiceSpy: jasmine.Spy;
  let postCommentServiceSpy: jasmine.Spy;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CommentService]
    });
    service = TestBed.inject(SharedCommentsService);
    commentService = TestBed.inject(CommentService);
    getCommentServiceSpy = spyOn(commentService, 'getComments').and.returnValue(of(mockComments));
    postCommentServiceSpy = spyOn(commentService, 'postComment').and.returnValue(of(mockComments[0]));
  });

  it('Load comments', () => {
    expect(service).toBeTruthy();
    service.loadComments(1);
    expect(getCommentServiceSpy).toHaveBeenCalled();
    service.lastestComments.subscribe(res => {
      expect(res.length).toBeGreaterThan(0);
    })
  });

  it('post comment', () => {
    expect(service).toBeTruthy();
    service.addComment(1, 'user', 'content').subscribe(res => {
      expect(res).toBeTruthy();
    });
    expect(postCommentServiceSpy).toHaveBeenCalled();
  });
});
