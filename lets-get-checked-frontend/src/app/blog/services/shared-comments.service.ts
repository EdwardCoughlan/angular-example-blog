import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Comment } from '../models/comment';
import { CommentService } from './comment.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})


export class SharedCommentsService {
  private comments = new BehaviorSubject<Comment[]>([]);
  public lastestComments = this.comments.asObservable();

  constructor(private service: CommentService) {
  }

  // not super happy with this. if you want to see comments per posts with multiple post
  // shown your going to have to rework the shared service.
  public loadComments(postId: number) {
    this.service.getComments(postId).pipe().subscribe(res => this.comments.next(res));
  }

  public addComment(postId: number, userName: string, comment: string): Observable<Comment> {
    const newComment: Comment = new Comment();
    newComment.user = userName;
    newComment.content = comment;
    newComment.parent_id = postId;
    newComment.postId = postId;
    const commentDate = new Date(Date.now());
    newComment.date = commentDate.toISOString().split('T')[0];
    return this.service.postComment(postId, newComment);
  }

}
