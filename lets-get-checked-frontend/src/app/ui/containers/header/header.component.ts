import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
    <nav>
      <a routerLink="/">
        <img 
          [attr.src]="logo"
          [attr.alt]="imageAltText"
          width="50"
        />
        <div class="nav-text">
          {{title}}
        </div>
      </a>
      
      <a routerLink="/blogs">
        <div class="nav-text">
          Posts
        </div>
      </a>
    </nav>
  `,
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public logo = 'assets/donut.svg';
  public imageAltText = 'Cartoon picture of a donut';
  public title = 'Super awesome blogs';
  constructor() { }

  ngOnInit(): void {
  }

}